<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/list', [\App\Http\Controllers\ImageController::class, "apiList"])
    ->name("api.list");

Route::get('/item/{id}', [\App\Http\Controllers\ImageController::class, "apiItem"])
    ->name("api.item");
