<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/upload', [\App\Http\Controllers\ImageController::class, "uploadForm"])->name("upload-form");
Route::post('/upload', [\App\Http\Controllers\ImageController::class, "upload"])->name("upload");
Route::get('/view', [\App\Http\Controllers\ImageController::class, "list"])->name("list");
Route::get('/zip/{image_name}', [\App\Http\Controllers\ImageController::class, "zip"])->name("zip");
