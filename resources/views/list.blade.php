@extends("layout.main")

@section("content")
    <link rel="stylesheet" href ="//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css"/>
    <script src = "https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="//cdn.datatables.net/2.0.3/js/dataTables.min.js" ></script>

    <h1>Список изображений</h1>

    <table id = "myTable">
        <thead>
            <th>ID</th>
            <th>Превью</th>
            <th>Название</th>
            <th>Время загрузки</th>
            <th>ZIP</th>
        </thead>
    @foreach($images as $img)
        <tr>
            <td>{{ $img->id }}</td>
            <td><a href = "{{ asset("storage/{$img->name}") }}" target="_blank"><img src = "{{ asset("storage/thumb/{$img->name}") }}"/></a></td>
            <td>{{ $img->name }}</td>
            <td>{{ $img->created_at }}</td>
            <td><a href = "{{ route("zip", ["image_name" => $img->name]) }}" target="_blank">DL</a></td>
        </tr>
    @endforeach
    </table>

    <script>let table = new DataTable('#myTable');</script>
@endsection
