@extends("layout.main")

@section("content")
    <ul>
        <li><a href ="{{ route("upload-form") }}">Upload Form</a></li>
        <li><a href ="{{ route("list") }}">View list</a></li>
        <li><a href ="{{ route("api.list") }}">Api List</a></li>
        <li><a href ="{{ route("api.item", ["id"=>1]) }}">Api by ID</a></li>
    </ul>
@endsection
