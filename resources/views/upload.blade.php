@extends("layout.main")

@section("content")

    <h1>Загрузка изображений</h1>
    <p>Выберите до 5 изображений для загрузки</p>
    <form method="POST" action = "{{ route("upload") }}" enctype="multipart/form-data" >
        @csrf
        <input type ="file" accept="image/*" name = "images[]" multiple="multiple" />

        <input type ="submit" value="Отправить" />

        @error('images')
            <div id = "error" style ="color:red;">{{ $message }}</div>
        @enderror

    </form>


@endsection
