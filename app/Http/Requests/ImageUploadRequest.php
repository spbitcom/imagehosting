<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'images' => 'required|max:5',
            'images.*' => 'image'
        ];
    }

    public function messages() : array
    {
        return [
            'required' => "Выберите для загрузки от одного до пяти изображений",
            'max' => 'Одновременно можно загрузить не более :max изображений',
            'image' => 'Допустима загрузка ТОЛЬКО изображений'
        ];
    }
}
