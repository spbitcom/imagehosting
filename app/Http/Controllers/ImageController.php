<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageUploadRequest;
use App\Models\Image;
use App\Services\ImageService;

class ImageController
{
    public function uploadForm() {
        return view("upload");
    }


    public function upload(ImageUploadRequest $request, ImageService $service) {

        foreach($request->images as $image_file) {
            $image = $service->saveUploadedImage($image_file);
            $service->makeThumbnail($image_file, $image->name);
        }

        return redirect()->route("list");
    }

    public function list()
    {
        $images = Image::all();
        return view("list", ["images" => $images]);
    }



    public function zip(string $image_name, ImageService $service)
    {
        $zip_name = $service->zip($image_name);
        return response()->download($zip_name);
    }

    public function apiList()
    {
        return Image::all();
    }

    public function apiItem(int $id)
    {
        return Image::find($id);
    }


}
