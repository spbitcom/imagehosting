<?php

namespace App\Services;

use App\Models\Image;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use ZipArchive;

class ImageService
{
    /**
     * @throws \Exception
     */
    public function saveUploadedImage(\Illuminate\Http\UploadedFile $image_file) : Image {
        $image = new Image;
        $prepared_name =
            Str::of($image_file->getClientOriginalName())
                ->ascii()
                ->lower();

        $image_exists = Image::where("name", $prepared_name)->exists();
        if($image_exists) {
            $prepared_name = Str::random(4) . " $prepared_name";
        }

        $image->name = $prepared_name;
        $image->save();

        $stored = $image_file->storeAs("public", $prepared_name);
        if(!$stored) {
            $image->forceDelete();
            throw new \Exception("file {$image_file->getClientOriginalName()} not saved");
        }
        return $image;
    }

    public function makeThumbnail(\Illuminate\Http\UploadedFile $image_file, string $filename) {

        $manager = ImageManager::imagick();
        $image = $manager->read($image_file);
        $image->scaleDown(150, 150);
        $image->save(public_path("storage/thumb/$filename"));

    }

    public function zip($image_name) {
        // TODO: zamenit na stream
        $zip = new ZipArchive();
        $zip_name = "$image_name.zip";
        $zip->open($zip_name, ZipArchive::CREATE);
        $zip->addFile(public_path("storage/$image_name"), $image_name);
        $zip->close();
        return $zip_name;
    }
}
